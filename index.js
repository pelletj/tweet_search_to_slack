var Twitter = require('twitter')
var IncomingWebhook = require('@slack/client').IncomingWebhook;
var fs = require('fs')

var twitterClient = new Twitter(JSON.parse(process.env.TWITTER_CREDENTIALS))
var slackWebhook = new IncomingWebhook(process.env.SLACK_WEBHOOK_URL);
var search = process.env.TWITTER_SEARCH
var tweet_store_path = '/data/tweet_store.json'
var tweet_store
var now = new Date();
var retention_date = now.setDate(now.getDate() - 7)

function check_tweet(tweet) {
  if (!tweet_store[tweet.id] && Date.parse(tweet.created_at) > retention_date) {
    tweet_store[tweet.id] = {
      created_at: tweet.created_at
    }
    return true
  }
  return false
}

function cleanup_tweet_store(){
  for (var k in tweet_store){
    console.log(tweet_store[k].created_at)
    if (Date.parse(tweet_store[k].created_at) < retention_date){
      delete tweet_store[k]
    }
  }
}

function slack_message(message) {
  slackWebhook.send(message, function(err, header, statusCode, body) {
    if (err) {
      console.log('Error:', err)
    } else {
      console.log('Received', statusCode, 'from Slack')
    }
  })
}

function load_tweet_store() {
  fs.readFile(tweet_store_path, 'utf8', function (error,data) {
    if (error) {
      if (error.code == 'ENOENT'){
        tweet_store = {}
      } else return console.log(error)
    } else {
      tweet_store = JSON.parse(data)
    }
  });
}

load_tweet_store();

twitterClient.get('search/tweets', {q: search}, function(error, tweets, response) {
  if (error) {
    return console.log(error)
  }

  for (var i=0; i< tweets.statuses.length; i++) {
    if (check_tweet(tweets.statuses[i])) {
      slack_message(tweets.statuses[i].text)
    }
  }

  cleanup_tweet_store()

  fs.writeFile(tweet_store_path, JSON.stringify(tweet_store), 'utf8', function (error) {if (error) {console.log(error)}});
});
